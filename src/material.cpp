#include "material.hpp"
#include "global.hpp"

Material::Material() : 
		diffuse("default.png"), 
		specular_color("default_specular_color.png"), 
		specular_factor("default_specular_factor.png"), 
		normal("default_normal.png"),
		bump("default_normal.png"),
		requires_blending(false),
		requires_alpha_test(false)
{
}

void Material::serialize(std::ostream& out)const
{
	Global::serialize(diffuse, out);
	Global::serialize(specular_color, out);
	Global::serialize(specular_factor, out);
	Global::serialize(shininess, out);
	Global::serialize(bump, out);
	Global::serialize(normal, out);
	Global::serialize(requires_blending, out);
	Global::serialize(requires_alpha_test, out);
}
void Material::serialize(std::istream& in)
{
	Global::serialize(diffuse, in);
	Global::serialize(specular_color, in);
	Global::serialize(specular_factor, in);
	Global::serialize(shininess, in);
	Global::serialize(bump, in);
	Global::serialize(normal, in);
	Global::serialize(requires_blending, in);
	Global::serialize(requires_alpha_test, in);
}

bool operator ==(const Material& left, const Material& right)
{
	return left.diffuse == right.diffuse &&
		left.specular_color == right.specular_color &&
		left.specular_factor == right.specular_factor &&
		left.shininess == right.shininess &&
		left.bump == right.bump &&
		left.normal == right.normal;
}
bool operator <(const Material& left, const Material& right)
{
	if(left.diffuse == right.diffuse){
		if(left.normal == right.normal){
			return left.specular_factor < right.specular_factor;
		}
		return left.normal < right.normal;
	}
	return left.diffuse < right.diffuse;
}

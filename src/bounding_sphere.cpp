#include <bounding_sphere.hpp>
#include <global.hpp>

BoundingSphere::BoundingSphere(const glm::vec3& position, Radius radius) : radius(radius), position(position)
{
}

BoundingSphere::BoundingSphere() : radius(0)
{
}
void BoundingSphere::serialize(std::ostream& out)const
{
	Global::serialize(position, out);
	Global::serialize(radius.radius, out);
}
void BoundingSphere::serialize(std::istream& in)
{
	Global::serialize(position, in);
	Global::serialize(radius.radius, in);
}

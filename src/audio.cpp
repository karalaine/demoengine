#include "audio.h"
namespace demo
{
	Audio::Audio()
	{
		if(!BASS_Init(-1, 44100, 0, 0, NULL))
		{
			std::cout << "Bass init failed: " << BASS_ErrorGetCode();
		}
	}
	Audio::~Audio()
	{
	}
	int Audio::loadSound(const std::string &fileName)
	{
		DWORD chan;
		if (!(chan=BASS_StreamCreateFile(FALSE,fileName.c_str(),0,0,0))) //streaming type files
		{
			if(!(chan=BASS_MusicLoad(FALSE,fileName.c_str(),0,0,BASS_MUSIC_RAMP,1))) //Mod music
			{
				std::cout << "Bass loading sound failed: " << BASS_ErrorGetCode();
			}
		}
		return chan;
	}
	int Audio::playSound(int channel)
	{
		return BASS_ChannelPlay(channel,FALSE);
	}
	int Audio::seekPoint(const float point)
	{
		return 0;
	}
	double Audio::getTime(DWORD chan)
	{
		return BASS_ChannelBytes2Seconds(chan, BASS_ChannelGetLength(chan, BASS_POS_BYTE)); // playback duration
	}
}
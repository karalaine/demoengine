#include "scene_node.hpp"
#include "global.hpp"
#include <stdint.h>

void SceneNode::serialize(std::ostream& out)const
{
	uint32_t meshes_size = meshes.size();
	uint32_t children_size = children.size();

	Global::serialize(name, out);
	Global::serialize(transformation, out);
	Global::serialize(meshes_size, out);
	out.write(reinterpret_cast<const char*>(meshes.data()),meshes_size*sizeof(int));
	Global::serialize(children_size, out);
	for(auto iter = children.begin(); iter != children.end(); ++iter)
		iter->serialize(out);
}
void SceneNode::serialize(std::istream& in)
{
	uint32_t meshes_size = 0;
	uint32_t children_size = 0;

	Global::serialize(name, in);
	Global::serialize(transformation, in);
	Global::serialize(meshes_size, in);
	meshes.resize(meshes_size);
	if(!meshes.empty())
		in.read(reinterpret_cast<char*>(meshes.data()),meshes_size*sizeof(int));
	Global::serialize(children_size, in);
	children.resize(children_size);
	for(auto iter = children.begin(); iter != children.end(); ++iter)
		iter->serialize(in);
}
#include "animation.hpp"
#include  <iostream>
#include <stdint.h>

float Animation::animation_fps=30.0f;

Animation::Animation() : 
	ticks(0),
	current_translation_index(0), 
	current_scale_index(0), 
	current_rotation_index(0),
	animation_length(0),
	current_anim_time(0)
{
}
Animation::Animation(Animation&& a) :
	affected_mesh(std::move(a.affected_mesh)),
	translations(std::move(a.translations)),
	scales(std::move(a.scales)),
	rotations(std::move(a.rotations)),
	transformations(std::move(a.transformations)),
	current_translation_index(a.current_translation_index),
	current_scale_index(a.current_scale_index),
	current_rotation_index(a.current_rotation_index),
	current_local_transformation(std::move(a.current_local_transformation)),
	current_global_transformation(std::move(a.current_global_transformation)),
	ticks(a.ticks),
	animation_length(a.animation_length),
	current_anim_time(a.current_anim_time)
{
}
Animation& Animation::operator=(Animation&& a)
{
	affected_mesh = std::move(a.affected_mesh);
	translations = std::move(a.translations);
	scales = std::move(a.scales);
	rotations = std::move(a.rotations);
	transformations = std::move(a.transformations);
	current_translation_index = a.current_translation_index;
	current_scale_index = a.current_scale_index;
	current_rotation_index = a.current_rotation_index;
	current_local_transformation = std::move(a.current_local_transformation);
	current_global_transformation = a.current_global_transformation;
	ticks = a.ticks;
	animation_length = a.animation_length;
	current_anim_time = a.current_anim_time;
	return *this;
}
void Animation::initialize(const aiNodeAnim &animation, float ticks_per_second, float animation_length)
{
	current_anim_time=0;
	float normalize_time = 1/ticks_per_second*animation_fps;
	auto& channel = animation;
	affected_mesh = channel.mNodeName.data;
	this->animation_length = animation_length;

	for(unsigned int k=0;k<channel.mNumPositionKeys;++k){
		auto& position_key = channel.mPositionKeys[k];
		float time = static_cast<float>(position_key.mTime*normalize_time);
		glm::vec3 translation = glm::vec3(position_key.mValue.x,position_key.mValue.z,position_key.mValue.y);
		translations.push_back(KeyMatrix(static_cast<float>(position_key.mTime/ticks_per_second), glm::translate(glm::mat4(), translation)));
	}
	for(unsigned int k=0;k<channel.mNumScalingKeys;++k){
		auto& scale_key = channel.mScalingKeys[k];
		float time = static_cast<float>(scale_key.mTime*normalize_time);
		glm::vec3 scale = glm::vec3(scale_key.mValue.x,scale_key.mValue.y,scale_key.mValue.z);
		scales.push_back(KeyMatrix(static_cast<float>(scale_key.mTime/ticks_per_second), glm::scale(glm::mat4(), scale)));
	}
	std::vector<aiQuatKey> quaternions;
	for(unsigned int k=0;k<channel.mNumRotationKeys;++k){
		auto& rotation_key = channel.mRotationKeys[k];
		float time = static_cast<float>(rotation_key.mTime);
		quaternions.push_back(rotation_key);
		quaternions.back().mTime = time;
	}

	for(auto iter = quaternions.begin();iter!=quaternions.end();++iter){
		glm::mat4 m = glm::mat4_cast(glm::quat(iter->mValue.w, iter->mValue.x, iter->mValue.y, iter->mValue.z));
		rotations.push_back(KeyMatrix(static_cast<float>(iter->mTime/ticks_per_second), m));
	}
	if(translations.empty())
		translations.push_back(KeyMatrix(0, glm::mat4()));
	if(scales.empty())
		scales.push_back(KeyMatrix(0, glm::mat4()));
	if(rotations.empty())
		rotations.push_back(KeyMatrix(0, glm::mat4()));
	if(transformations.empty())
		transformations.push_back(KeyMatrix(0, glm::mat4()));
	current_translation_index = 0;
	current_scale_index = 0;
	current_rotation_index = 0;
}
void Animation::update(const Time& animation_time)
{
	current_anim_time +=animation_time.asSeconds();
	
	current_local_transformation = get_local_transformation(seconds(std::fmod(current_anim_time ,animation_length)));
	current_global_transformation = get_global_transformation(seconds(std::fmod(current_anim_time ,animation_length)));
}

glm::mat4 Animation::get_local_transformation(const Time& animation_time)
{
	if(transformations.size() > 1)
		return transformations[find_transformation_key(animation_time.asSeconds())].matrix;
	
	auto rotation_key_index = find_rotation_key(animation_time.asSeconds());
	auto translation_key_index = find_translation_key(animation_time.asSeconds());
	auto scale_key_index = find_scale_key(animation_time.asSeconds());

	return translations[translation_key_index].matrix * rotations[rotation_key_index].matrix * scales[scale_key_index].matrix;
}
glm::mat4 Animation::get_local_transformation()const
{
	return current_local_transformation;
}
glm::mat4 Animation::get_global_transformation(const Time& animation_time)
{
	if(transformations.size() > 1)
		return transformations[find_transformation_key(animation_time.asSeconds())].global_matrix;
	
	auto rotation_key_index = find_rotation_key(animation_time.asSeconds());
	auto translation_key_index = find_translation_key(animation_time.asSeconds());
	auto scale_key_index = find_scale_key(animation_time.asSeconds());

	return translations[translation_key_index].global_matrix * rotations[rotation_key_index].global_matrix * scales[scale_key_index].global_matrix;
}
glm::mat4 Animation::get_global_transformation()const
{
	return current_global_transformation;
}
std::size_t Animation::find_rotation_key(float animation_time)const
{
	for(std::size_t i=0; i != rotations.size(); ++i)
		if(rotations[i].time_s > animation_time)
			return i;
	return 0;
}
std::size_t Animation::find_translation_key(float animation_time)const
{
	for(std::size_t i=0; i != translations.size(); ++i)
		if(translations[i].time_s > animation_time)
			return i;
	return 0;
}
std::size_t Animation::find_scale_key(float animation_time)const
{
	for(std::size_t i=0; i != scales.size(); ++i)
		if(scales[i].time_s > animation_time)
			return i;
	return 0;
}
std::size_t Animation::find_transformation_key(float animation_time)const
{
	for(std::size_t i=0; i != transformations.size(); ++i)
		if(transformations[i].time_s > animation_time)
			return i;
	return 0;
}
void Animation::serialize(std::ostream& out)const
{
	Global::serialize(affected_mesh, out);
	Global::serialize(translations.size(), out);
	out.write(reinterpret_cast<const char*>(translations.data()),sizeof(KeyMatrix)*translations.size());
	Global::serialize(scales.size(), out);
	out.write(reinterpret_cast<const char*>(scales.data()),sizeof(KeyMatrix)*scales.size());
	Global::serialize(rotations.size(), out);
	out.write(reinterpret_cast<const char*>(rotations.data()),sizeof(KeyMatrix)*rotations.size());
	Global::serialize(transformations.size(), out);
	out.write(reinterpret_cast<const char*>(transformations.data()),sizeof(KeyMatrix)*transformations.size());

	out.write(reinterpret_cast<const char*>(&ticks),sizeof(ticks));
	out.write(reinterpret_cast<const char*>(&animation_length),sizeof(animation_length));
	out.write(reinterpret_cast<const char*>(&current_anim_time),sizeof(current_anim_time));
}
void Animation::serialize(std::istream& in)
{
	Global::serialize(affected_mesh, in);
	uint32_t translations_size=0;
	uint32_t scales_size=0;
	uint32_t rotations_size=0;
	uint32_t transformations_size=0;

	Global::serialize(translations_size, in);
	translations.resize(translations_size);
	if(!translations.empty())
		in.read(reinterpret_cast<char*>(&translations[0]),sizeof(KeyMatrix)*translations.size());
	Global::serialize(scales_size, in);
	scales.resize(scales_size);
	if(!scales.empty())
		in.read(reinterpret_cast<char*>(&scales[0]),sizeof(KeyMatrix)*scales.size());
	Global::serialize(rotations_size, in);
	rotations.resize(rotations_size);
	if(!rotations.empty())
		in.read(reinterpret_cast<char*>(&rotations[0]),sizeof(KeyMatrix)*rotations.size());
	Global::serialize(transformations_size, in);
	transformations.resize(transformations_size);
	if(!transformations.empty())
		in.read(reinterpret_cast<char*>(&transformations[0]),sizeof(KeyMatrix)*transformations.size());

	in.read(reinterpret_cast<char*>(&ticks),sizeof(ticks));
	in.read(reinterpret_cast<char*>(&animation_length),sizeof(animation_length));
	in.read(reinterpret_cast<char*>(&current_anim_time),sizeof(current_anim_time));
}


#include "vertex.hpp"

bool fast_vector_compare(const glm::vec3& left, const glm::vec3& right)
{
	bool sign = glm::sign(left) == glm::sign(right);
	if(sign){
		auto absed = glm::abs(left-right);
		return absed.x < 0.001 && absed.y < 0.001 && absed.z < 0.001;
	}
	return false;
}
bool fast_vector_compare(const glm::vec2& left, const glm::vec2& right)
{
	bool sign = glm::sign(left) == glm::sign(right);
	if(sign){
		auto absed = glm::abs(left-right);
		return absed.x < 0.001 && absed.y < 0.001;
	}
	return false;
}
bool fast_vector_compare(const glm::vec4& left, const glm::vec4& right)
{
	bool sign = glm::sign(left) == glm::sign(right);
	if(sign){
		auto absed = glm::abs(left-right);
		return absed.x < 0.001 && absed.y < 0.001 && absed.z < 0.001 && absed.w < 0.001;
	}
	return false;
}
bool operator ==(const Vertex& left, const Vertex& right)
{
	return fast_vector_compare(left.vertex, right.vertex) &&
		fast_vector_compare(left.normal, right.normal) &&
		fast_vector_compare(left.tex_coord, right.tex_coord) &&
		fast_vector_compare(left.tangent, right.tangent) &&
		left.bone_ids==right.bone_ids &&
		fast_vector_compare(left.bone_weights, right.bone_weights);
	/*
	return glm::distance(left.vertex, right.vertex) < 0.001 &&
		glm::distance(left.normal, right.normal) < 0.001 &&
		glm::distance(left.tex_coord, right.tex_coord) < 0.001 &&
		glm::distance(left.tangent, right.tangent) < 0.001 &&
		left.bone_ids==right.bone_ids &&
		glm::distance(left.bone_weights, right.bone_weights) < 0.001;
	*/
}
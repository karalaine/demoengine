#include "Engine.h"

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <fstream>
#include "audio.h"

namespace demo
{
	Engine::Engine(void)
	{

	}

	

	Engine::~Engine(void)
	{

	}
	void Engine::render()
	{
		
		glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		// clear the buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	void Error(HWND windowHandle, std::string mesg)
	{
		MessageBoxA(windowHandle, mesg.c_str(),0,0);
	}

	bool promptMessage(HWND windowHandle, std::string question)
	{
		int msgboxID = MessageBoxA(windowHandle, question.c_str(),0,MB_YESNO|MB_ICONQUESTION);
		switch (msgboxID)
		{
		case IDNO:
			return false;
			break;
		case IDYES:
		   return true;
			break;
		case IDCANCEL:
			return false;
			break;
		default:
			return false;
		}
	}

	int Engine::run()
	{
		sf::ContextSettings settings;
		settings.depthBits = 24;
		settings.stencilBits = 8;
		settings.antialiasingLevel = 4;
		settings.majorVersion = 4;
		settings.minorVersion = 3;
		bool fullScreen = promptMessage(NULL,"Run in fullscreen?");
		sf::Window window(sf::VideoMode(1280, 720), "SFML works!", fullScreen ? sf::Style::Fullscreen : sf::Style::Default,settings);
		gl3wInit();
		const GLchar *vertex_shader[] = {
		"void main(void) {\n",
		"    gl_Position = vec4(1.0);",
		"}"
		};

		const GLchar *color_shader[] = {
		"void main() {\n",
		"    gl_FragColor = vec4(1.0,1.0,1.0,1.0);\n",
		"}"
		};
		Shader exampleShader(vertex_shader, color_shader);
		window.setVerticalSyncEnabled(true);
		settings = window.getSettings();
#ifdef _DEBUG
		std::cout << "depth bits:" << settings.depthBits << std::endl;
		std::cout << "stencil bits:" << settings.stencilBits << std::endl;
		std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
		std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;
#endif
		demo::Audio song;
		//Error(window.getSystemHandle(), "Here is popup");
		//int handle = song.loadSound("sieni.mp3");
		if(0)
		{
			//song.playSound(handle);
		}
		else
		{
			std::cout << "Audio loading failed";
		}
		bool running = true;
		while (window.isOpen() && running)
		{
			// handle events
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					// end the program
					running = false;
				}
				else if (event.type == sf::Event::Resized)
				{
					// adjust the viewport when the window is resized
					glViewport(0, 0, event.size.width, event.size.height);
				}
				else if (event.type == sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::Escape)
					{
						window.close();
					}
				}
			}
		
			// draw...
			render();
            exampleShader();
			// end the current frame (internally swaps the front and back buffers)
			window.display();
		}

		return 0;
	}
}

#include "mesh.hpp"
#include "global.hpp"

void Mesh::serialize(std::ostream& out)const
{
	Global::serialize(begin, out);
	Global::serialize(count, out);
	Global::serialize(material_index, out);
	Global::serialize(has_bones, out);
}
void Mesh::serialize(std::istream& in)
{
	Global::serialize(begin, in);
	Global::serialize(count, in);
	Global::serialize(material_index, in);
	Global::serialize(has_bones, in);
}
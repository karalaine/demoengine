#include "global.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <limits>
#include <cctype>
#include <algorithm>

namespace Global
{
	std::string extract_filename(const std::string& file)
	{
		std::size_t dot_position = file.find_last_of('.');
		if(dot_position != std::string::npos){
			for(auto iter = file.rbegin(); iter != file.rend();++iter){
				if(*iter == '\\' || *iter == '/'){
					std::size_t start_position = std::distance(iter,file.rend());
					return file.substr(start_position,dot_position-start_position);
				}
			}
			return file.substr(0,dot_position);
		}
		return file;
	}
	template<>
	glm::vec3 lexical_cast<glm::vec3>(const std::string& data)
	{
		std::stringstream ss(data);
		glm::vec3 value;
		if(!(ss >> value.x >> value.y >> value.z))
			std::cerr << "Failed to lexical_cast: " << data << " to glm::vec3" << std::endl;
		return value;
	}
	float angle_degrees(const glm::vec3& a, const glm::vec3& b)
	{
		using namespace glm;
		return degrees(acos(dot(normalize(a), normalize(b))));
	}
	glm::vec3 rotate(const glm::vec3& original_direction, const glm::vec3& target_direction, float rotation_speed_in_degrees, Time time)
	{
		float target_angle = glm::degrees(std::atan2(target_direction.x, target_direction.z));
		float current_angle = glm::degrees(std::atan2(original_direction.x, original_direction.z));
		float angle_difference = glm::distance(current_angle, target_angle);
		float angle_difference2 = angle_degrees(target_direction, original_direction);
		float rotation = glm::sign(current_angle-target_angle)*glm::min(rotation_speed_in_degrees*time.asSeconds(), angle_difference2);

		if(std::min(angle_difference2, angle_difference) > 1){
			//simulation run!
			auto simulated_movement_direction = original_direction * glm::mat3(glm::rotate(glm::mat4(), rotation, glm::vec3(0,1,0)));
			float simulated_angle_difference = angle_degrees(target_direction, simulated_movement_direction);
			//We turned the wrong way, AARGHHH
			if(simulated_angle_difference > angle_difference2)
				rotation *=-1;
			return original_direction * glm::mat3(glm::rotate(glm::mat4(),rotation,glm::vec3(0,1,0)));
		}
		return original_direction;
	}
	void remove_duplicates(std::vector<std::string>& data)
	{
		std::sort(data.begin(), data.end());
		auto iter = std::unique(data.begin(),data.end());
		data.resize(iter-data.begin());
	}
	std::string extract_filename_and_type(const std::string& file)
	{
		for(auto iter = file.rbegin(); iter != file.rend();++iter)
			if(*iter == '\\' || *iter == '/')
				return file.substr(std::distance(iter, file.rend()));
		return file;
	}
	std::string extract_filetype(const std::string& file)
	{
		auto pos = file.find_last_of('.');
		if(pos != std::string::npos)
			return file.substr(pos);
		return "";
	}
	void trim_trailing_carriage_returns(std::string& string)
	{
		while(string.size() > 0 && string.back() == '\r')
			string.pop_back();
	}

	std::string extract_everything_after(const std::string& line, const std::string& after)
	{
		std::size_t pos = line.find(after);
		if(pos != std::string::npos)
			return line.substr(pos+after.size());
		return "";
	}
	std::string extract_everything_before(const std::string& line, const std::string& before)
	{
		auto pos = line.find(before);
		if(pos != std::string::npos)
			return line.substr(0, pos);
		return line;
	}

	std::string to_string(const glm::vec3& p)
	{
		std::stringstream ss;
		ss << std::fixed << std::setprecision(5) << p.x << ", " << p.y << ", " << p.z;
		return ss.str();
	}
	std::string to_string(const glm::vec4& p)
	{
		std::stringstream ss;
		ss << std::fixed << std::setprecision(5) << p.x << ", " << p.y << ", " << p.z << ", " << p.w;
		return ss.str();
	}
	std::string to_string(const glm::mat4& p)
	{
		std::stringstream ss;
		for(int i=0;i != 4;++i)
			ss << to_string(p[i]) << std::endl;
		return ss.str();
	}
	std::size_t find_next_not_numerical(const std::string& from, std::size_t begin)
	{
		for(std::size_t i = begin;begin < from.size();++i)
			if(!std::isdigit(from[i]) && from[i] != '-')
				return i;
		return std::string::npos;
	}
	std::pair<glm::vec3,glm::vec3> minmax_element(const std::vector<glm::vec3>& data)
	{
		glm::vec3 min = glm::vec3(std::numeric_limits<float>::max());
		glm::vec3 max = glm::vec3(std::numeric_limits<float>::min());
		for(auto iter = data.begin();iter != data.end();++iter){
			if(iter->x < min.x)
				min.x = iter->x;
			if(iter->y < min.y)
				min.y = iter->y;
			if(iter->z < min.z)
				min.z = iter->z;

			if(iter->x > max.x)
				max.x = iter->x;
			if(iter->y > max.y)
				max.y = iter->y;
			if(iter->z > max.z)
				max.z = iter->z;
		}
		return std::make_pair(min, max);
	}
	void serialize(const std::string& str, std::ostream& out)
	{
		uint32_t str_length = str.length();
		out.write(reinterpret_cast<const char*>(&str_length), sizeof(str_length));
		out.write(str.data(), str_length);
	}
	void serialize(std::string& str, std::istream& in)
	{
		uint32_t str_length = str.length();
		in.read(reinterpret_cast<char*>(&str_length), sizeof(str_length));
		str.resize(str_length);
		if(str_length != 0)
			in.read(&str[0], str_length);
	}

    std::string to_lower(const std::string& string)
    {
        std::string str = string;
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        return str;
    }

};

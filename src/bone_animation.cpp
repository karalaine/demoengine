#include "bone_animation.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <cstdint>
#include <algorithm>


BoneAnimation::BoneAnimation() :
	animation_length(0),
	current_animation_time(0)
{
}

glm::mat4 BoneAnimation::find_transformation_key(float animation_time)const
{
	auto transformation = std::upper_bound(transformations.begin(), transformations.end(), animation_time, []
		(const float& animation_time, const KeyMatrix& m)
	{ 
		return m.time > animation_time; 
	});
	if(transformation == transformations.end())
		return transformations.back().matrix;
	return transformation->matrix;
}
void BoneAnimation::update(const Time& timestep)
{
	current_animation_time += timestep.asSeconds();
	current_transformation = get_transformation(seconds(current_animation_time));
}
glm::mat4 BoneAnimation::get_transformation(const Time& animation_time)
{
	return find_transformation_key(std::fmod(animation_time.asSeconds() ,animation_length));
}
glm::mat4 BoneAnimation::get_transformation()const
{
	return current_transformation;
}

void BoneAnimation::KeyMatrix::serialize(std::ostream& out)const
{
	out.write(reinterpret_cast<const char*>(&time), sizeof(time));
	out.write(reinterpret_cast<const char*>(glm::value_ptr(matrix)), sizeof(matrix));
}
void BoneAnimation::KeyMatrix::serialize(std::istream& in)
{
	in.read(reinterpret_cast<char*>(&time), sizeof(time));
	in.read(reinterpret_cast<char*>(glm::value_ptr(matrix)), sizeof(matrix));
}

void BoneAnimation::serialize(std::ostream& out)const
{
	std::uint32_t name_length = bone_name.length();
	std::uint32_t transformations_size = transformations.size();
	out.write(reinterpret_cast<const char*>(&name_length), sizeof(name_length));
	out.write(reinterpret_cast<const char*>(bone_name.data()), name_length);
	out.write(reinterpret_cast<const char*>(&transformations_size), sizeof(transformations_size));
	for(auto iter = transformations.begin(); iter != transformations.end(); ++iter)
		iter->serialize(out);
	out.write(reinterpret_cast<const char*>(&animation_length), sizeof(animation_length));
	out.write(reinterpret_cast<const char*>(&current_animation_time), sizeof(current_animation_time));
}
void BoneAnimation::serialize(std::istream& in)
{
	std::uint32_t name_length = bone_name.length();
	std::uint32_t transformations_size = transformations.size();
	in.read(reinterpret_cast<char*>(&name_length), sizeof(name_length));
	bone_name.resize(name_length);
	in.read(reinterpret_cast<char*>(&bone_name[0]), name_length);
	in.read(reinterpret_cast<char*>(&transformations_size), sizeof(transformations_size));
	transformations.resize(transformations_size);
	for(auto iter = transformations.begin(); iter != transformations.end(); ++iter)
		iter->serialize(in);
	in.read(reinterpret_cast<char*>(&animation_length), sizeof(animation_length));
	in.read(reinterpret_cast<char*>(&current_animation_time), sizeof(current_animation_time));
}


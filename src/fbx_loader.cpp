#include "fbx_loader.hpp"
#include "global.hpp"
#include "animation.hpp"
#include <string>
#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <fstream>
#include <memory>

glm::mat4 to_glm(const FbxAMatrix& in)
{
	return glm::mat4(
		in[0][0], in[0][1], in[0][2], in[0][3],
		in[1][0], in[1][1], in[1][2], in[1][3],
		in[2][0], in[2][1], in[2][2], in[2][3],
		in[3][0], in[3][1], in[3][2], in[3][3]);
}
glm::vec4 to_glm(const FbxVector4& in)
{
	return glm::vec4(in.mData[0], in.mData[1], in.mData[2], in.mData[3]);
}
glm::vec3 to_glm(const FbxDouble3& in)
{
	return glm::vec3(in.mData[0], in.mData[1], in.mData[2]);
}
glm::vec2 to_glm(const FbxVector2& in)
{
	return glm::vec2(in.mData[0], in.mData[1]);
}

template<class T>
class scoped_fbx_resource
{
public:
	scoped_fbx_resource(T* resource) : resource(resource)
	{
	}
	~scoped_fbx_resource(){
		resource->Destroy();
	}
	T* resource;
private:
	scoped_fbx_resource(const scoped_fbx_resource&);
	scoped_fbx_resource& operator=(const scoped_fbx_resource&);
};

FbxLoader::FbxLoader()
{
}
std::vector<std::string> FbxLoader::get_texture_filenames(FbxProperty &pProperty)
{
	int layered_texture_count = pProperty.GetSrcObjectCount<FbxLayeredTexture>();
	std::vector<std::string> texture_names;

	if(layered_texture_count > 0){
		for(int j=0; j != layered_texture_count; ++j){
			FbxLayeredTexture *layered_texture = pProperty.GetSrcObject<FbxLayeredTexture>(j);
			if(!layered_texture)
				return texture_names;
			FbxFileTexture* file_texture = FbxCast<FbxFileTexture>(layered_texture);
			if(!file_texture)
				return texture_names;
			texture_names.push_back(Global::extract_filename_and_type(file_texture->GetFileName()));
		}
	}
	else{
		int texture_count = pProperty.GetSrcObjectCount();

		if(texture_count > 0){
			for(int j=0; j != texture_count; ++j){
				FbxTexture* texture = pProperty.GetSrcObject<FbxTexture>(j);
				FbxFileTexture* file_texture = FbxCast<FbxFileTexture>(texture);
				if(file_texture)
					texture_names.push_back(Global::extract_filename_and_type(file_texture->GetFileName()));
			}
		}
	}
	return texture_names;
}
int FbxLoader::get_material_id(const FbxMesh& mesh, int triangle_index)
{
	int element_material_count = mesh.GetElementMaterialCount();
	if(element_material_count > 1)
		std::clog << "More than 1 material per element?" << std::endl;
	for (int j = 0; j != element_material_count; ++j){
		const FbxGeometryElementMaterial* material_element = mesh.GetElementMaterial(j);
		FbxSurfaceMaterial* material  = mesh.GetNode()->GetMaterial(material_element->GetIndexArray().GetAt(triangle_index));
		int material_id = material_element->GetIndexArray().GetAt(triangle_index);
		std::string name = material->GetName();
		/*
		if(!name.empty())
			std::cout << mesh.GetName() << " = " << name << material_id << std::endl;

		return material_id;
		*/
		return materials_string_to_index[name];
	}
	return -1;
}

std::unique_ptr<FbxGeometryConverter> converter;
std::vector<Vertex> FbxLoader::load_control_points(const FbxMesh& mesh)
{
	FbxVector4* control_points = mesh.GetControlPoints();
	int control_point_count = mesh.GetControlPointsCount();
//	int cp = converter->TriangulateMesh(&mesh)->GetControlPointsCount();
	std::vector<Vertex> vertices;
	vertices.reserve(control_point_count);
	for(auto iter = control_points; iter != control_points+control_point_count;++iter){
		Vertex v;
		v.vertex = glm::vec3(to_glm(*iter));
		vertices.push_back(v);
	}
	return vertices;
}
Mesh3 FbxLoader::load_mesh(const FbxMesh& mesh, const std::vector<Vertex>& control_points)
{
	if(!mesh.IsTriangleMesh()){
		std::cerr << "Cant process meshes that are not triangulated, polygon size:" << std::endl;
		throw std::runtime_error("Mesh not triangulated");
		return Mesh3();
	}
	int triangle_count = mesh.GetPolygonCount();
	int control_point_count = mesh.GetControlPointsCount();

	Mesh3 mesh2;
	mesh2.vertices.reserve(triangle_count*3);

	for(int triangle_index=0; triangle_index != triangle_count;++triangle_index){
		int material_id = get_material_id(mesh, triangle_index);
		for(int triangle_element_index=0; triangle_element_index != 3; ++triangle_element_index){ //for each triangle corner
			int indice = mesh.GetPolygonVertex(triangle_index, triangle_element_index);
			
			Vertex vertex;
			//vertex.vertex = glm::vec3(to_glm(mesh.GetControlPointAt(indice)));
			vertex = control_points[indice];

			//get normal
			FbxVector4 normal;
			mesh.GetPolygonVertexNormal(triangle_index, triangle_element_index, normal);
			vertex.normal = glm::normalize(glm::vec3(to_glm(normal)));
			
			// Get texture coordinate
			FbxVector2 texture_coordinate = FbxVector2(0.0, 0.0);
			const FbxLayerElementUV* fbxLayerUV = mesh.GetLayer(0)->GetUVs();
			if(fbxLayerUV){
				int iUVIndex = 0;
				switch (fbxLayerUV->GetMappingMode()){
					case FbxLayerElement::eByControlPoint:
						iUVIndex = indice;
						break;
					case FbxLayerElement::eByPolygonVertex:
						iUVIndex = const_cast<FbxMesh&>(mesh).GetTextureUVIndex(triangle_index, triangle_element_index, FbxLayerElement::eTextureDiffuse);
						break;
				}
				texture_coordinate = fbxLayerUV->GetDirectArray().GetAt(iUVIndex);
			}
			vertex.tex_coord = to_glm(texture_coordinate);
			
			//get tangent
			glm::vec3 tangent;
			const FbxLayerElementTangent* tangent_layer = mesh.GetLayer(0)->GetTangents();
			if(!tangent_layer){
				std::cerr << "Mesh:" << mesh.GetNode()->GetName() << " is missing tangents" << std::endl;
				assert(tangent_layer);
				throw std::runtime_error("Mesh:" + std::string(mesh.GetName()) + " is missing tangents");
			}
			auto tangent_mapping_mode = tangent_layer->GetMappingMode();
			switch(tangent_mapping_mode){
				case FbxLayerElement::eByControlPoint:
					tangent = glm::vec3(to_glm(tangent_layer->GetDirectArray().GetAt(indice)));
					break;
				case FbxLayerElement::eByPolygonVertex:
				{
					auto tangent_element = mesh.GetElementTangent(0);
					auto tangent_mapping_mode = tangent_element->GetMappingMode();
					if(tangent_mapping_mode == FbxGeometryElement::eByPolygonVertex){
						switch(tangent_element->GetReferenceMode()){
							case FbxGeometryElement::eDirect:
								tangent = glm::vec3(to_glm(tangent_element->GetDirectArray().GetAt(triangle_index*3+triangle_element_index)));
								break;
							case FbxGeometryElement::eIndexToDirect:
							{
								int id = tangent_element->GetIndexArray().GetAt(triangle_index*3+triangle_element_index);
								tangent = glm::vec3(to_glm(tangent_element->GetDirectArray().GetAt(id)));
								break;
							}
						}
					}
					break;
				}
			}
			vertex.tangent = tangent;
			auto duplicate = find_duplicate(mesh2, vertex);
			if(duplicate == mesh2.vertices.end()){
				mesh2.indices[material_id].push_back(mesh2.vertices.size());
				mesh2.vertices.push_back(vertex);
			}
			else
				mesh2.indices[material_id].push_back(std::distance(static_cast<std::vector<Vertex>::const_iterator>(mesh2.vertices.begin()), duplicate));
		}
	}
	return mesh2;
}

std::vector<Vertex>::const_iterator FbxLoader::find_duplicate(const Mesh3& from, Vertex& what)const
{
	for(auto iter = from.vertices.begin(); iter != from.vertices.end();++iter){
		if(*iter == what)
			return iter;
	}
	return from.vertices.end();
}

inline void FbxLoader::map_bone_to_vertex(Vertex& vertex, int id, float weight)const
{
	for(int i=0;i != 4;++i){
		if(vertex.bone_weights[i] == 0.0){
			vertex.bone_weights[i] = static_cast<float>(weight);
			vertex.bone_ids[i] = id;
			break;
		}
	}
}
void FbxLoader::map_bones_to_vertex(const std::vector<Bone>& bones, Vertex& vertex, int old_indice)
{
	int bone_index=0;
	for(auto bone = bones.begin(); bone != bones.end(); ++bone,++bone_index){
		auto weight = std::find_if(bone->vertex_weights.begin(), bone->vertex_weights.end(),[=](const VertexBoneData& vbd){return vbd.vertex_id == old_indice; });
		if(weight != bone->vertex_weights.end())
			map_bone_to_vertex(vertex, bone_index, weight->weight);
	}
}
glm::mat4 get_node_geometry_transform(const FbxNode& node)
{
	auto translation = node.GetGeometricTranslation(FbxNode::eSourcePivot);
	auto scale = node.GetGeometricScaling(FbxNode::eSourcePivot);
	auto rotation = node.GetGeometricRotation(FbxNode::eSourcePivot);

	return to_glm(FbxAMatrix(translation, rotation, scale));
}
FbxAMatrix get_node_geometry_transform_fbx(const FbxNode& node)
{
	auto translation = node.GetGeometricTranslation(FbxNode::eSourcePivot);
	auto scale = node.GetGeometricScaling(FbxNode::eSourcePivot);
	auto rotation = node.GetGeometricRotation(FbxNode::eSourcePivot);

	return FbxAMatrix(translation, rotation, scale);
}
std::vector<Bone> FbxLoader::load_and_map_bones(const FbxGeometry& geometry, std::vector<Vertex>& control_points)
{
	std::vector<Bone> bones;

	int skin_count=geometry.GetDeformerCount(FbxDeformer::eSkin);
	for(int i=0; i != skin_count; ++i)
	{
		int cluster_count = ((FbxSkin *) geometry.GetDeformer(i, FbxDeformer::eSkin))->GetClusterCount();
		for (int j = 0; j != cluster_count; ++j)
		{
			Bone bone;
			FbxCluster* cluster=((FbxSkin *) geometry.GetDeformer(i, FbxDeformer::eSkin))->GetCluster(j);

			if(cluster->GetLink() != NULL)
				bone.name = cluster->GetLink()->GetName();

			int indices_count = cluster->GetControlPointIndicesCount();
			int* indices = cluster->GetControlPointIndices();
			double* weights = cluster->GetControlPointWeights();
			
			for(int k = 0; k != indices_count; k++)
			{
				int indice = indices[k];
				float weight = (float)weights[k];
				map_bone_to_vertex(control_points[indice],j,weight);
				//bone.vertex_weights.push_back(VertexBoneData(indice, weight));
			}

			FbxAMatrix matrix;
			matrix = cluster->GetTransformMatrix(matrix);
			bone.transform = to_glm(matrix);
			matrix = cluster->GetTransformLinkMatrix(matrix);
			bone.transform_link = to_glm(matrix);
			
			auto global_matrix = bone.transform * get_node_geometry_transform(*geometry.GetNode());
			
			if (cluster->GetAssociateModel() != NULL){
				matrix = cluster->GetTransformAssociateModelMatrix(matrix);
				bone.transform_associate_model = to_glm(matrix);
			}

			bone.bind_matrix = glm::inverse(glm::inverse(global_matrix) * bone.transform_link);
			auto cluster_global_initial_position = cluster->GetTransformLinkMatrix(matrix).Inverse();
			auto reference_global_initial_position = cluster->GetTransformMatrix(matrix) * get_node_geometry_transform_fbx(*geometry.GetNode());
			auto bind_matrix2 = to_glm(cluster_global_initial_position * reference_global_initial_position);
			bone.bind_matrix = to_glm(cluster_global_initial_position * reference_global_initial_position);
			
			bones.push_back(bone);
		}
	}
	return bones;
}
std::vector<Bone> FbxLoader::load_bones(const FbxGeometry& geometry) //aka mesh
{
	std::vector<Bone> bones;

	int skin_count=geometry.GetDeformerCount(FbxDeformer::eSkin);
	for(int i=0; i != skin_count; ++i)
	{
		int cluster_count = ((FbxSkin *) geometry.GetDeformer(i, FbxDeformer::eSkin))->GetClusterCount();
		for (int j = 0; j != cluster_count; ++j)
		{
			Bone bone;
			FbxCluster* cluster=((FbxSkin *) geometry.GetDeformer(i, FbxDeformer::eSkin))->GetCluster(j);

			if(cluster->GetLink() != NULL)
				bone.name = cluster->GetLink()->GetName();

			int indices_count = cluster->GetControlPointIndicesCount();
			int* indices = cluster->GetControlPointIndices();
			double* weights = cluster->GetControlPointWeights();
			
			for(int k = 0; k != indices_count; k++)
			{
				int indice = indices[k];
				float weight = (float)weights[k];
				bone.vertex_weights.push_back(VertexBoneData(indice, weight));
			}

			FbxAMatrix matrix;
			matrix = cluster->GetTransformMatrix(matrix);
			bone.transform = to_glm(matrix);
			matrix = cluster->GetTransformLinkMatrix(matrix);
			bone.transform_link = to_glm(matrix);
			
			auto global_matrix = bone.transform * get_node_geometry_transform(*geometry.GetNode());
			
			if (cluster->GetAssociateModel() != NULL){
				matrix = cluster->GetTransformAssociateModelMatrix(matrix);
				bone.transform_associate_model = to_glm(matrix);
			}

			bone.bind_matrix = glm::inverse(glm::inverse(global_matrix) * bone.transform_link);
			auto cluster_global_initial_position = cluster->GetTransformLinkMatrix(matrix).Inverse();
			auto reference_global_initial_position = cluster->GetTransformMatrix(matrix) * get_node_geometry_transform_fbx(*geometry.GetNode());
			auto bind_matrix2 = to_glm(cluster_global_initial_position * reference_global_initial_position);
			bone.bind_matrix = to_glm(cluster_global_initial_position * reference_global_initial_position);
			
			bones.push_back(bone);
		}
	}
	return bones;
}
void FbxLoader::load_mesh(const FbxNode& node)
{
	const FbxMesh* mesh = reinterpret_cast<const FbxMesh*>(node.GetNodeAttribute());
	if(mesh){
		
		auto control_points = load_control_points(*mesh);
		auto bones = load_and_map_bones(*mesh, control_points);
		meshes.emplace_back(load_mesh(*mesh, control_points));
		meshes.back().bones = bones;
	}
}

void FbxLoader::traverse_node(const FbxNode& node, SceneNode& parent)
{
	SceneNode child_node;
	
	const char* node_name = node.GetName();
	child_node.name = node_name;
	FbxTime time;
	time.SetSecondDouble(0.0);
	auto transformation = to_glm(const_cast<FbxNode&>(node).EvaluateLocalTransform(time));
	child_node.transformation = transformation;

	bool is_usefull_node=false;
	for(int i = 0; i < node.GetNodeAttributeCount(); ++i){
		if(node.GetNodeAttributeByIndex(i)){
			if(node.GetNodeAttributeByIndex(i)->GetAttributeType() == FbxNodeAttribute::eMesh){
				load_mesh(node);
				child_node.meshes.push_back(meshes.size()-1);
				is_usefull_node = true;
			}
			else if(node.GetNodeAttributeByIndex(i)->GetAttributeType() == FbxNodeAttribute::eSkeleton)
				is_usefull_node = true;
			else{
				
			}
		}
	}
	if(is_usefull_node)
		parent.children.push_back(child_node);
	// Recursively traverse the children nodes.
	for(int j = 0; j < node.GetChildCount(); ++j)
		traverse_node(*node.GetChild(j), is_usefull_node ? parent.children.back() : parent);
}

void FbxLoader::load_material( const FbxSurfaceMaterial& material, Material& material_out)
{
	FbxProperty lProperty;
	//Diffuse Textures
	lProperty = material.FindProperty(FbxSurfaceMaterial::sDiffuse);
	auto diffuse_map = get_texture_filenames(lProperty);
	if(!diffuse_map.empty())
		material_out.diffuse = Global::to_lower(diffuse_map.front());
	/*
	//DiffuseFactor Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuseFactor);
	get_texture_names(lProperty);

	//Emissive Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sEmissive);
	get_texture_names(lProperty);

	//EmissiveFactor Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sEmissiveFactor);
	get_texture_names(lProperty);

	//Ambient Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sAmbient);
	get_texture_names(lProperty); 

	//AmbientFactor Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sAmbientFactor);
	get_texture_names(lProperty);          
	*/
	//Specular Textures
	lProperty = material.FindProperty(FbxSurfaceMaterial::sSpecular);
	auto specular_map = get_texture_filenames(lProperty);  
	if(!specular_map.empty())
		material_out.specular_color = Global::to_lower(specular_map.front());

	//SpecularFactor Textures
	lProperty = material.FindProperty(FbxSurfaceMaterial::sSpecularFactor);
	auto specular_factor_map = get_texture_filenames(lProperty);
	if(!specular_factor_map.empty())
		material_out.specular_factor = Global::to_lower(specular_factor_map.front());

	//Shininess Textures
	lProperty = material.FindProperty(FbxSurfaceMaterial::sShininess);
	auto shininess_map = get_texture_filenames(lProperty);
	if(!shininess_map.empty())
		material_out.shininess = Global::to_lower(shininess_map.front());

	//Bump Textures
	lProperty = material.FindProperty(FbxSurfaceMaterial::sBump);
	auto bump_map = get_texture_filenames(lProperty);
	if(!bump_map.empty())
		material_out.bump = Global::to_lower(bump_map.front());

	//Normal Map Textures
	lProperty = material.FindProperty(FbxSurfaceMaterial::sNormalMap);
	auto normal_map = get_texture_filenames(lProperty);
	if(!normal_map.empty())
		material_out.normal = Global::to_lower(normal_map.front());
	/*
	//Transparent Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sTransparentColor);
	get_texture_names(lProperty);

	//TransparencyFactor Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sTransparencyFactor);
	get_texture_names(lProperty);

	//Reflection Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sReflection);
	get_texture_names(lProperty);

	//ReflectionFactor Textures
	lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sReflectionFactor);
	get_texture_names(lProperty);
	*/
}
void FbxLoader::load_materials(FbxScene& scene)
{
	int material_count = scene.GetMaterialCount();
	//materials2.resize(material_count);
	for(int i=0;i != material_count ; ++i){
		auto material = scene.GetMaterial(i);
		std::string name = material->GetName();
		if(material){
			load_material(*material, materials[i]);
			if(name.find("blend") != std::string::npos || name.find("glass") != std::string::npos)
				materials[i].requires_blending = true;
			if(name.find("alpha") != std::string::npos)
				materials[i].requires_alpha_test = true;
			materials_string_to_index[name] = i;
		}
	}
}

std::vector<std::map<int, Material>::const_iterator> FbxLoader::find_duplicate_materials(const std::map<int, Material> &materials, const Material& reference_material)
{
	std::vector<std::map<int, Material>::const_iterator> duplicates;
	for(auto iter2 = materials.begin(); iter2 != materials.end();++iter2){
		if(iter2->second == reference_material){
			duplicates.push_back(iter2);
		}
	}
	return duplicates;
}
std::map<int, std::vector<unsigned short>>::iterator FbxLoader::find_mesh_using_material(int material_id)
{
	std::map<int, std::vector<unsigned short>>::iterator mesh_iter;
	for(auto iter2 = meshes.begin(); iter2 != meshes.end();++iter2){
		mesh_iter = iter2->indices.find(material_id);
		if(mesh_iter != iter2->indices.end())
			break;
	}
	return mesh_iter;
}
void FbxLoader::remove_duplicate_materials(std::map<int, Material> &materials, std::vector<Mesh3> &meshes)
{
	//for each material
	for(auto iter = materials.begin(); iter != materials.end();++iter){
		//find duplicates aka find materials that use exact same textures
		auto duplicates = find_duplicate_materials(materials, iter->second);

		if(duplicates.size() > 1){
			//find mesh that uses currently handled material
			//we'll dump all the meshes that use the material into this one
			auto mesh_iter = find_mesh_using_material(iter->first);
			
			//remove all duplicates(leave one) 
			for(auto iter2 = duplicates.begin(); iter2 != duplicates.end();++iter2){
				if(*iter2 == iter)
					continue;

				//fix meshes that use duplicate materials
				for(auto iter3 = meshes.begin(); iter3 != meshes.end();++iter3){
					auto result = iter3->indices.find((**iter2).first);
					if(result != iter3->indices.end()){
						for(auto iter = result->second.begin(); iter != result->second.end();++iter)
							mesh_iter->second.push_back(*iter);
						iter3->indices.erase(result);
					}
				}
				//remove duplicate
				materials.erase(*iter2);
			}
		}
	}
}


std::vector<BoneAnimation> FbxLoader::load_bone_animations(const FbxScene& scene)
{
	std::vector<BoneAnimation> animations;
	auto root = scene.GetRootNode();
	if(!root)
		return animations;
	for(int i = 0; i != root->GetChildCount(); ++i){
		auto sub_animations = load_bone_animations(*root, *root->GetChild(i));
		std::copy(sub_animations.begin(), sub_animations.end(), std::back_inserter<std::vector<BoneAnimation>>(animations));
	}
	return animations;
}
std::vector<BoneAnimation> FbxLoader::load_bone_animations(const FbxNode& parent, const FbxNode& node)
{
	std::vector<BoneAnimation> animations;

	const char* node_name = node.GetName();
	FbxTime time;
	time.SetSecondDouble(0.0);
	auto transformation = to_glm(const_cast<FbxNode&>(node).EvaluateLocalTransform(time));

	bool is_usefull_node=false;
	for(int i = 0; i < node.GetNodeAttributeCount(); ++i){
		if(node.GetNodeAttributeByIndex(i) && node.GetNodeAttributeByIndex(i)->GetAttributeType() == FbxNodeAttribute::eMesh){
			const FbxMesh* mesh = reinterpret_cast<const FbxMesh*>(node.GetNodeAttribute());
			auto sub_animations = load_bone_animations(*mesh);
			std::copy(sub_animations.begin(), sub_animations.end(), std::back_inserter<std::vector<BoneAnimation>>(animations));
		}
	}

	// Recursively traverse the children nodes.
	for(int i = 0; i != node.GetChildCount(); ++i){
		auto sub_animations = load_bone_animations(node, *node.GetChild(i));
		std::copy(sub_animations.begin(), sub_animations.end(), std::back_inserter<std::vector<BoneAnimation>>(animations));
	}
	return animations;
}
std::vector<BoneAnimation> FbxLoader::load_bone_animations(const FbxGeometry& geometry)
{
	std::vector<BoneAnimation> animations;
	FbxAnimStack *animation_stack=nullptr;
	try_find_animation_affecting_geometry(*geometry.GetScene(), *geometry.GetNode(), animation_stack);
	if(!animation_stack)
		return animations;
	auto animation_timespan = animation_stack->GetReferenceTimeSpan();
	float animation_length = static_cast<float>(animation_timespan.GetDuration().GetSecondDouble());
	float animation_timestep = 1/60.0f;

	int skin_count=geometry.GetDeformerCount(FbxDeformer::eSkin);
	for(int i=0; i != skin_count; ++i)
	{
		int cluster_count = ((FbxSkin *) geometry.GetDeformer(i, FbxDeformer::eSkin))->GetClusterCount();
		for (int j = 0; j != cluster_count; ++j)
		{
			BoneAnimation bone_animation;
			FbxCluster* cluster=((FbxSkin *) geometry.GetDeformer(i, FbxDeformer::eSkin))->GetCluster(j);

			if(cluster->GetLink() != NULL)
				bone_animation.bone_name = cluster->GetLink()->GetName();

			FbxCluster::ELinkMode cluster_mode = cluster->GetLinkMode();

			//right static side
			FbxAMatrix matrix;
			auto cluster_global_initial_position = cluster->GetTransformLinkMatrix(matrix).Inverse();
			auto reference_global_initial_position = cluster->GetTransformMatrix(matrix);
			reference_global_initial_position *= get_node_geometry_transform_fbx(*geometry.GetNode());
			//bone_animation.cluster_relative_initial_position = to_glm(cluster_global_initial_position * reference_global_initial_position);
			
			
			//left side
			FbxAMatrix geometry_offset = get_node_geometry_transform_fbx(*geometry.GetNode());
			const FbxNode& node = *geometry.GetNode();
			for(float i=0; i < animation_length; i+= animation_timestep){
				FbxTime time;
				time.SetSecondDouble(i);
				FbxAMatrix global_position = const_cast<FbxNode&>(node).EvaluateGlobalTransform(time);
				FbxAMatrix global_off_position = global_position * geometry_offset;
				FbxAMatrix reference_global_current_position_inverse = global_off_position.Inverse();
				FbxAMatrix cluster_global_current_position = cluster->GetLink()->EvaluateGlobalTransform(time);
				FbxAMatrix cluster_relative_current_position_inverse = reference_global_current_position_inverse * cluster_global_current_position;
				bone_animation.transformations.emplace_back(BoneAnimation::KeyMatrix(i,to_glm(cluster_relative_current_position_inverse)));
			}
			bone_animation.animation_length = animation_length;
			animations.emplace_back(bone_animation);
		}
	}
	return animations;
}

bool FbxLoader::has_meshes(const SceneNode& scene_node, const FbxNode& node)
{
	std::string node_name = node.GetName();
	if(scene_node.name == node_name)
		return !scene_node.meshes.empty();
	for(auto iter = scene_node.children.begin(); iter != scene_node.children.end();++iter){
		if(has_meshes(*iter, node))
			return true;
	}
	return false;
}
std::vector<Animation::Key> FbxLoader::load_animation_keys(FbxAnimCurve& pCurve, float animation_length)const
{
	std::vector<Animation::Key> keys;
	float time_step = 1/60.0f;
	float frame=0;
	for(float time=0; time < animation_length; time+=time_step,++frame){
		FbxTime fbxtime;
		fbxtime.SetSecondDouble(time);
		keys.push_back(Animation::Key(pCurve.Evaluate(fbxtime),time));
	}
	return keys;
}

std::vector<Animation::KeyMatrix> FbxLoader::load_channel_transformation_keys(FbxNode& node, FbxAnimLayer& animation_layer, float animation_length)const
{
	std::vector<Animation::KeyMatrix> keys;
	float time_step = 1/60.0f;

	keys.reserve(static_cast<std::size_t>(animation_length/time_step)+1);
	FbxTime fbxtime;
	for(float time=0; time < animation_length; time+=time_step){
		fbxtime.SetSecondDouble(time);
		keys.push_back(Animation::KeyMatrix(time, to_glm(node.EvaluateLocalTransform(fbxtime))));
		keys.back().global_matrix = to_glm(node.EvaluateGlobalTransform(fbxtime));
	}
	if(keys.empty()){
		keys.push_back(Animation::KeyMatrix(0, to_glm(node.EvaluateLocalTransform())));
		keys.back().global_matrix = to_glm(node.EvaluateGlobalTransform());
	}
	return keys;
}

Animation FbxLoader::load_animation_channel(FbxNode& node, FbxAnimLayer& animation_layer, float animation_length)const
{
	Animation channel;
	channel.transformations = load_channel_transformation_keys(node, animation_layer, animation_length);

	channel.affected_mesh= node.GetName();
	channel.animation_length = animation_length;
	return channel;
}

void FbxLoader::load_animation_layer(FbxAnimLayer& animation_layer, FbxNode& node, std::vector<Animation>& animations, float animation_length)
{
	if(has_meshes(root, node))
		animations.emplace_back(load_animation_channel(node, animation_layer, animation_length));
	for(int i = 0; i != node.GetChildCount(); ++i)
		load_animation_layer(animation_layer, *node.GetChild(i), animations, animation_length);
}
void FbxLoader::load_animation_stack(FbxAnimStack& animation_stack, FbxNode& node, std::vector<Animation>& animations)
{
	int animation_layers_count = animation_stack.GetMemberCount<FbxAnimLayer>();
	auto animation_timespan = animation_stack.GetReferenceTimeSpan();
	float animation_length = static_cast<float>(animation_timespan.GetDuration().GetSecondDouble());
	animations.reserve(animation_layers_count);
	for (int i = 0; i != animation_layers_count; ++i){
		FbxAnimLayer& animation_layer = *animation_stack.GetMember<FbxAnimLayer>(i);
		load_animation_layer(animation_layer, node, animations, animation_length );
	}
}
std::vector<Animation> FbxLoader::load_animations(FbxScene& scene)
{
	std::vector<Animation> animations;
	int animation_count = scene.GetSrcObjectCount<FbxAnimStack>();
	animations.reserve(animation_count);
	for (int i = 0; i != animation_count; ++i){
		FbxAnimStack& animation_stack= *scene.GetSrcObject<FbxAnimStack>(i);
		
		load_animation_stack(animation_stack, *scene.GetRootNode(), animations);
	}
	return animations;
}
//not sure if this makes sense or not
void FbxLoader::try_find_animation_affecting_geometry(const FbxScene& scene, const FbxNode& node, FbxAnimStack*& result)
{
	int animation_count = scene.GetSrcObjectCount<FbxAnimStack>();
	for (int i = 0; i != animation_count; ++i){
		FbxAnimStack& animation_stack= *scene.GetSrcObject<FbxAnimStack>(i);
		result = scene.GetSrcObject<FbxAnimStack>(i);
		return;
	}
}
bool FbxLoader::contains_node(const SceneNode& node, const std::string& nodename)
{
	if(node.name == nodename)
		return true;
	for(auto iter = node.children.begin(); iter != node.children.end();++iter){
		if(contains_node(*iter, nodename))
			return true;
	}
	return false;
}
float distance(const glm::mat4& left, const glm::mat4& right)
{
	return glm::distance(left[0], right[0]) 
		+ glm::distance(left[1], right[1]) 
		+ glm::distance(left[2], right[2]) 
		+ glm::distance(left[3], right[3]);
}
bool FbxLoader::all_key_transformations_identical(const std::vector<Animation::KeyMatrix>& keys)
{
	if(keys.size() < 2)
		return true;
	for(auto iter = keys.begin(); iter != keys.end()-1; ++iter){
		if(distance(iter->matrix, (iter+1)->matrix) > 0.0001 || distance(iter->global_matrix, (iter+1)->global_matrix))
			return false;
	}
	return true;
}
void FbxLoader::remove_animations_that_dont_affect_scene(std::vector<Animation> &animations, const SceneNode& root)
{
	for(auto iter = animations.begin(); iter != animations.end();){
		if(!contains_node(root, iter->affected_mesh) || (iter->rotations.empty() && iter->translations.empty() && iter->scales.empty() && iter->transformations.empty())
			|| all_key_transformations_identical(iter->transformations))
			iter = animations.erase(iter);
		else
			++iter;
	}
}

SceneNode* FbxLoader::find_node_using_mesh(SceneNode& root, int mesh_index)
{
	for(auto iter = root.meshes.begin(); iter != root.meshes.end();++iter)
		if(*iter == mesh_index)
			return &root;
	for(auto iter = root.children.begin(); iter != root.children.end();++iter){
		auto result = find_node_using_mesh(*iter, mesh_index);
		if(result)
			return result;
	}
	return nullptr;
}

std::vector<Mesh> FbxLoader::combine_meshes(std::vector<Mesh3>& meshes)
{
	//std::vector<Bone> bones;
	//std::vector<BoneHandle> bone_handles;

	int vertex_count=0;
	int indice_count=0;
	int bone_count=0;

	//count end vertices, indices and bones count
	for(auto iter = meshes.begin(); iter != meshes.end();++iter){
		vertex_count += iter->vertices.size();
		bone_count += iter->bones.size();
		for(auto iter2 = iter->indices.begin(); iter2 != iter->indices.end();++iter2)
			indice_count += iter2->second.size();
	}
	vertices.reserve(vertex_count);
	indices.reserve(indice_count);
	bones.reserve(bone_count);
	bone_handles.reserve(bone_count);
	int current_indice_offset=0;
	int current_bone_offset=0;
	int mesh_index=0;
	for(auto iter = meshes.begin(); iter != meshes.end(); ++iter,++mesh_index){
		//advance vertice bone ids and copy 
		for(int i=0;i != iter->vertices.size();++i)
			iter->vertices[i].bone_ids += current_bone_offset;
		std::copy(iter->vertices.begin(), iter->vertices.end(), std::back_inserter(vertices)); 
		//find scenenode that uses this mesh
		SceneNode* node = find_node_using_mesh(root, mesh_index);
		if(node)
			node->meshes.clear();
		for(auto iter2 = iter->indices.begin();iter2 != iter->indices.end();++iter2){
			if(mesh_handles.empty())
				mesh_handles.push_back(Mesh(0, iter2->second.size(), iter2->first, !iter->bones.empty()));
			else
				mesh_handles.push_back(Mesh(mesh_handles.back().begin+mesh_handles.back().count, iter2->second.size(), iter2->first, !iter->bones.empty()));
			if(node)
				node->meshes.push_back(-1*(mesh_handles.size()-1)); //note the -1, we fix this in the end of this function
			//advance and copy indices
			for(int i=0;i != iter2->second.size(); ++i){
				//iter->indices[i] += current_indice_offset;
				iter2->second[i] += current_indice_offset;
				indices.emplace_back(iter2->second[i]);
			}
		}
		//advance and copy indices used in bones
		for(int i=0;i != iter->bones.size(); ++i){
			for(int j=0; j != iter->bones[i].vertex_weights.size();++j){
				iter->bones[i].vertex_weights[j].vertex_id += current_indice_offset;
			}
			bones.emplace_back(iter->bones[i]);
			bone_handles.emplace_back(BoneHandle(iter->bones[i].name, iter->bones[i].bind_matrix));
		}
		current_indice_offset += iter->vertices.size();
		current_bone_offset += iter->bones.size();
		
	}
	fix_scene_after_mesh_combine(root);
	return mesh_handles;
}
void FbxLoader::fix_scene_after_mesh_combine(SceneNode& node)
{
	for(auto iter = node.meshes.begin(); iter != node.meshes.end();++iter)
		*iter = -1**iter;
	for(auto iter = node.children.begin(); iter != node.children.end();++iter)
		fix_scene_after_mesh_combine(*iter);
}
void FbxLoader::serialize(std::ostream& out)const
{
	uint32_t mesh_handles_size = mesh_handles.size();
	uint32_t bone_animations_size = bone_animations.size();
	uint32_t materials_size = materials2.size();
	uint32_t animations_size = animations.size();
	uint32_t bone_handles_size = bone_handles.size();
	uint32_t vertices_size = vertices.size();
	uint32_t indices_size = indices.size();


	root.serialize(out);
	Global::serialize(mesh_handles_size, out);
	for(auto iter = mesh_handles.begin(); iter != mesh_handles.end(); ++iter)
		iter->serialize(out);
	Global::serialize(bone_handles_size, out);
	for(auto iter = bone_animations.begin(); iter != bone_animations.end(); ++iter)
		iter->serialize(out);
	Global::serialize(materials_size, out);
	for(auto iter = materials2.begin(); iter != materials2.end(); ++iter)
		iter->serialize(out);
	Global::serialize(animations_size, out);
	for(auto iter = animations.begin(); iter != animations.end(); ++iter)
		iter->serialize(out);
	
	Global::serialize(bone_handles_size, out);
	for(auto iter = bone_handles.begin(); iter != bone_handles.end(); ++iter)
		iter->serialize(out);
	Global::serialize(global_inverse_transformation, out);

	Global::serialize(vertices_size, out);
	out.write(reinterpret_cast<const char*>(vertices.data()), vertices.size()*sizeof(Vertex));
	Global::serialize(indices_size, out);
	out.write(reinterpret_cast<const char*>(indices.data()), indices.size()*sizeof(unsigned short));
	Global::serialize(bounding_box.min, out);
	Global::serialize(bounding_box.max, out);
}
void FbxLoader::serialize(std::istream& in)
{
	uint32_t mesh_handles_size = 0;
	uint32_t bone_animations_size = 0;
	uint32_t materials_size = 0;
	uint32_t animations_size = 0;
	uint32_t bone_handles_size = 0;
	uint32_t vertices_size = 0;
	uint32_t indices_size = 0;

	root.serialize(in);
	Global::serialize(mesh_handles_size, in);
	mesh_handles.resize(mesh_handles_size);
	for(auto iter = mesh_handles.begin(); iter != mesh_handles.end(); ++iter)
		iter->serialize(in);
	Global::serialize(bone_animations_size, in);
	bone_animations.resize(bone_animations_size);
	for(auto iter = bone_animations.begin(); iter != bone_animations.end(); ++iter)
		iter->serialize(in);
	Global::serialize(materials_size, in);
	materials2.resize(materials_size);
	for(auto iter = materials2.begin(); iter != materials2.end(); ++iter)
		iter->serialize(in);
	Global::serialize(animations_size, in);
	animations.resize(animations_size);
	for(auto iter = animations.begin(); iter != animations.end(); ++iter)
		iter->serialize(in);
	
	Global::serialize(bone_handles_size, in);
	bone_handles.resize(bone_handles_size);
	for(auto iter = bone_handles.begin(); iter != bone_handles.end(); ++iter)
		iter->serialize(in);
	Global::serialize(global_inverse_transformation, in);

	Global::serialize(vertices_size, in);
	vertices.resize(vertices_size);
	in.read(reinterpret_cast<char*>(vertices.data()), vertices.size()*sizeof(Vertex));
	Global::serialize(indices_size, in);
	indices.resize(indices_size);
	in.read(reinterpret_cast<char*>(indices.data()), indices.size()*sizeof(unsigned short));
	Global::serialize(bounding_box.min, in);
	Global::serialize(bounding_box.max, in);
}
void FbxLoader::clear()
{
	*this = FbxLoader();
}
void FbxLoader::get_min_max(const SceneNode& node, glm::vec3& min, glm::vec3& max)const
{
	for(auto iter = node.children.begin(); iter != node.children.end(); ++iter){
		get_min_max(*iter, min, max);
	}
	for(auto iter = node.meshes.begin(); iter != node.meshes.end(); ++iter){
		auto& mesh = mesh_handles[*iter];
		for(auto iter = mesh.begin; iter != mesh.begin+mesh.count; ++iter){
		//for(auto iter = mesh.vertices.begin(); iter != mesh.vertices.end(); ++iter){
			auto vertex = node.transformation*glm::vec4(vertices[indices[iter]].vertex, 1);

			if(vertex.x < min.x)
				min.x = vertex.x;
			if(vertex.y < min.y)
				min.y = vertex.y;
			if(vertex.z < min.z)
				min.z = vertex.z;

			if(vertex.x > max.x)
				max.x = vertex.x;
			if(vertex.y > max.y)
				max.y = vertex.y;
			if(vertex.z > max.z)
				max.z = vertex.z;
		}
	}
}
void FbxLoader::load_fbx(const std::string& filename)
{
	clear();
	std::ifstream cache_in_file("cache/"+Global::extract_filename_and_type(filename)+".dat", std::ios_base::binary);
	if(cache_in_file.is_open()){
		serialize(cache_in_file);
		return;
	}
	scoped_fbx_resource<FbxManager> sdk(FbxManager::Create());
	// Create the io settings object.
	auto *ios = FbxIOSettings::Create(sdk.resource, IOSROOT);
	ios->SetBoolProp(IMP_UP_AXIS, true);
	ios->SetBoolProp(IOSN_TANGENTS_BINORMALS, true);
	sdk.resource->SetIOSettings(ios);

	scoped_fbx_resource<FbxImporter> importer(FbxImporter::Create(sdk.resource,""));

	if(!importer.resource->Initialize(filename.c_str(), -1, sdk.resource->GetIOSettings())) {
		std::cerr << "Call to FbxImporter::Initialize() failed" << std::endl;
		//std::cerr << "Error returned: " <<  importer.resource->GetLastErrorString() << std::endl;
		assert(false);
		throw std::runtime_error("Fbx importer failed to import file" + filename);
	}
	scene = FbxScene::Create(sdk.resource,"scene");
	importer.resource->Import(scene);
	
	auto axis_system = scene->GetGlobalSettings().GetAxisSystem();
	FbxAxisSystem::OpenGL.ConvertScene(scene);
	FbxSystemUnit::m.ConvertScene(scene);
	converter = std::unique_ptr<FbxGeometryConverter>(new FbxGeometryConverter(sdk.resource));
	converter->Triangulate(scene,true);

	load_materials(*scene);
	
	FbxNode* RootNode = scene->GetRootNode();

	if(!RootNode)
		return;
	
	RootNode->ResetPivotSetAndConvertAnimation();
	
	global_inverse_transformation = to_glm(RootNode->EvaluateGlobalTransform().Inverse());
	root.name = RootNode->GetName();

	for(int i = 0; i != RootNode->GetChildCount(); ++i)
		traverse_node(*RootNode->GetChild(i), root);


	//remove_duplicate_materials(materials, meshes);
	materials2.resize(materials.size());
	for(auto iter = materials.begin(); iter != materials.end();++iter){
		if(static_cast<int>(materials2.size()) < iter->first+1)
			materials2.resize(iter->first+1);
		materials2[iter->first] = iter->second;
	}
	animations = load_animations(*scene);
	remove_animations_that_dont_affect_scene(animations, root);
	
	mesh_handles = combine_meshes(meshes);
	bone_animations = load_bone_animations2(*scene);

	if(bone_handles.size() != bone_animations.size()){
		std::clog << "Bone animations and bone handles size does not match, model might not show correctly" << std::endl;
		return;
	}
	for(int i=0;i != bone_handles.size(); ++i)
		bone_animations[i].bone_name = bone_handles[i].name;

	glm::vec3 min = glm::vec3(std::numeric_limits<float>::max());
	glm::vec3 max = glm::vec3(std::numeric_limits<float>::lowest());
	get_min_max(root, min, max);
	bounding_box = BoundingBox(min, max);

	std::ofstream cache_out_file("cache/"+Global::extract_filename_and_type(filename)+".dat", std::ios_base::binary);
	if(cache_out_file.is_open()){
		serialize(cache_out_file);
	}
	else
		std::clog << "Failed to cache file: " << "cache/"+Global::extract_filename_and_type(filename)+".dat" << std::endl;
}

#include "fbx_sdk_sample.hpp"

std::vector<BoneAnimation> FbxLoader::load_bone_animations2(FbxScene& scene)
{
	int animation_count = scene.GetSrcObjectCount<FbxAnimStack>();
	FbxAnimLayer* anim_layer=nullptr;
	float animation_length = 0;

	for (int i = 0; i != animation_count; ++i){
		FbxAnimStack& animation_stack= *scene.GetSrcObject<FbxAnimStack>(i);
		int animation_layers_count = animation_stack.GetMemberCount<FbxAnimLayer>();
		auto animation_timespan = animation_stack.GetReferenceTimeSpan();
		animation_length = static_cast<float>(animation_timespan.GetDuration().GetSecondDouble());
		for (int i = 0; i != animation_stack.GetMemberCount<FbxAnimLayer>(); ++i){
			anim_layer = animation_stack.GetMember<FbxAnimLayer>(i);
			break;
		}
	}
	
	std::vector<BoneAnimation> bone_animations;
	
	FbxAMatrix global_position;
	int last_printed_percent=-1;
	for(float i=0;i < animation_length;i+=1/60.0f){
		FbxTime time;
		time.SetSecondDouble(i);
		auto matrices = DrawNodeRecursive(scene.GetRootNode(), time, anim_layer, global_position, nullptr, SHADING_MODE_SHADED);
		if(matrices.size() > bone_animations.size())
			bone_animations.resize(matrices.size());
		for(int i=0; i!= matrices.size(); ++i)
			bone_animations[i].transformations.push_back(BoneAnimation::KeyMatrix(static_cast<float>(time.GetSecondDouble()), to_glm(matrices[i])));
	}
	for(auto iter = bone_animations.begin(); iter != bone_animations.end();++iter)
		iter->animation_length = animation_length;
	return bone_animations;
}

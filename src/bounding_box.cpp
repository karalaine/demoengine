#include "bounding_box.hpp"


BoundingBox::BoundingBox() : min(0.0f),max(1.0f)
{
}
BoundingBox::BoundingBox(const glm::vec3& min, const glm::vec3& max) :
min(min),max(max)
{
}
glm::vec3 BoundingBox::get_size()const
{ 
	return glm::abs(max-min);
}
glm::vec3 BoundingBox::get_middle_position()const
{ 
	return (min+max)/glm::vec3(2.0f);
}
glm::vec3 BoundingBox::get_foot_position()const
{
	glm::vec3 middle = get_middle_position();
	middle.y = min.y;
	return middle;
}

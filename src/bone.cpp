#include "bone.hpp"
#include "global.hpp"

void BoneHandle::serialize(std::ostream& out)const
{
	Global::serialize(name, out);
	Global::serialize(offset, out);
	Global::serialize(transformation, out);
}
void BoneHandle::serialize(std::istream& in)
{
	Global::serialize(name, in);
	Global::serialize(offset, in);
	Global::serialize(transformation, in);
}
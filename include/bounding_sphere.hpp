#ifndef BOUNDING_SPHERE_HPP
#define BOUNDING_SPHERE_HPP
#include <glm/glm.hpp>
#include <ostream>

struct Radius
{
	explicit Radius(float radius) : radius(radius){}
	float radius;
};
struct BoundingSphere
{
	BoundingSphere();
	//safe interface?
	explicit BoundingSphere(const glm::vec3& position, Radius radius);
	Radius radius;
	glm::vec3 position;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);
};

#endif

#ifndef BOUNDING_BOX_HPP
#define BOUNDING_BOX_HPP
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <limits>

struct BoundingBox
{
	BoundingBox();
	BoundingBox(const glm::vec3& min, const glm::vec3& max);
	glm::vec3 get_size()const;
	glm::vec3 get_middle_position()const;
	glm::vec3 get_foot_position()const;


	glm::vec3 min, max;
	glm::quat rotation;//tmp
	glm::mat4 transformation;//tmp
};

#endif

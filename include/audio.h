#include <bass.h>
#include <iostream>

namespace demo
{
	class Audio {
	public:
		Audio();
		virtual ~Audio();
		int loadSound(const std::string &fileName);
		int playSound(int channel);
		int seekPoint(const float point);
		double getTime(DWORD chan);
	};

}
#ifndef ANIMATION_HPP
#define ANIMATION_HPP
#include "time.hpp"
#include "global.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <assimp/scene.h>
#include <string>
#include <vector>


class Animation
{
public:
	Animation();
	Animation(Animation&& a);
	Animation& operator=(Animation&& a);
	void initialize(const aiNodeAnim &animation, float ticks_per_second, float animation_length);
	void update();
	void update(const Time& animation_time);
	glm::mat4 get_local_transformation(const Time& animation_time);
	glm::mat4 get_local_transformation()const;
	glm::mat4 get_global_transformation(const Time& animation_time);
	glm::mat4 get_global_transformation()const;
	std::string affected_mesh; //temporary
	static float animation_fps;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);

private:
	std::size_t find_rotation_key(float animation_time)const;
	std::size_t find_translation_key(float animation_time)const;
	std::size_t find_scale_key(float animation_time)const;
	std::size_t find_transformation_key(float animation_time)const;

	struct Key
	{
		Key(float value, float time) : value(value), time(time){}
		float value;
		float time;
	};
	struct KeyVector
	{
		KeyVector() : time_s(0){}
		KeyVector(float time, float time_s, glm::vec3 vector) : vector(vector),time_s(time_s){}
		float time_s; //seconds
		glm::vec3 vector;
	};
	struct KeyMatrix
	{
		KeyMatrix():time_s(0){}
		KeyMatrix(float time_s, glm::mat4 matrix) : matrix(matrix), time_s(time_s){}
		float time_s; //seconds
		glm::mat4 matrix;
		glm::mat4 global_matrix;
	};
	struct KeyQuaternion
	{
		KeyQuaternion():time_s(0){}
		KeyQuaternion(float time, float time_s, glm::quat quaternion) : quaternion(quaternion), time_s(time_s){}
		float time_s; //seconds
		glm::quat quaternion;
	};
	std::vector<KeyMatrix> translations, scales, rotations, transformations;
	
	std::size_t current_translation_index, current_scale_index, current_rotation_index;

	glm::mat4 current_local_transformation, current_global_transformation;

	int ticks;
	float animation_length;
	float current_anim_time;

	friend class FbxLoader;
};



#endif

#ifndef MESH_HPP
#define MESH_HPP
#include "bone.hpp"
#include "vertex.hpp"
#include <iostream>
#include <vector>
#include <map>

struct Mesh
{
	Mesh() : begin(0), count(0), material_index(0), has_bones(false){}
	Mesh(int begin, int count,int material_index, bool has_bones) : 
		begin(begin),count(count),material_index(material_index), has_bones(has_bones)
	{
	}
	int begin, count; //data begin and size in vertex buffer
	int material_index;
	bool has_bones;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);
};

struct Mesh2
{
	std::vector<Vertex> vertices;
	std::vector<unsigned short> indices;
	std::vector<Bone> bones;
	unsigned int material_id;
};
struct Mesh3
{
	std::vector<Vertex> vertices;
	//material id & indices
	std::map<int, std::vector<unsigned short>> indices;
	std::vector<Bone> bones;
};

#endif

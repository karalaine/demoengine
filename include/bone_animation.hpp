#ifndef BONE_ANIMATION_HPP
#define BONE_ANIMATION_HPP
#include "time.hpp"
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <iostream>

class BoneAnimation
{
public:
	BoneAnimation();
	void update(const Time& animation_time);
	glm::mat4 get_transformation(const Time& animation_time);
	glm::mat4 get_transformation()const;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);

	std::string bone_name;
private:
	glm::mat4 find_transformation_key(float animation_time)const;

	struct KeyMatrix
	{
		KeyMatrix():time(0){}
		KeyMatrix(float time, glm::mat4 matrix) : time(time), matrix(matrix){}
		float time; //seconds
		glm::mat4 matrix;
		void serialize(std::ostream& out)const;
		void serialize(std::istream& in);
	};
	
	std::vector<KeyMatrix> transformations;
	glm::mat4 current_transformation;

	float animation_length;
	float current_animation_time;

	friend class FbxLoader;
};

#endif

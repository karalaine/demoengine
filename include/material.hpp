#ifndef MATERIAL_HPP
#define MATERIAL_HPP
#include <string>
#include <iostream>

struct Material
{
	Material();
	std::string diffuse, specular_color, specular_factor, shininess, bump, normal;
	bool requires_blending, requires_alpha_test;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);
};
bool operator ==(const Material& left, const Material& right);
bool operator <(const Material& left, const Material& right);

#endif

#ifndef FBX_LOADER_HPP
#define FBX_LOADER_HPP
#include "mesh.hpp"
#include "bone.hpp"
#include "scene_node.hpp"
#include "vertex.hpp"
#include "material.hpp"
#include "animation.hpp"
#include "bone_animation.hpp"
#include "bounding_box.hpp"
#include "bounding_sphere.hpp"

#define FBXSDK_NEW_API
#include <fbxsdk.h>
#undef FBXSDK_NEW_API // I dont like macros :|

#include <map>
#include <vector>
#include <string>

class FbxLoader
{
public:
	FbxLoader();
	void load_fbx(const std::string& filename);
	std::map<int, Material> materials;
	std::map<std::string, int> materials_string_to_index;
	std::vector<Material> materials2;

	std::vector<Mesh> mesh_handles;
	std::vector<Vertex> vertices;
	std::vector<unsigned short> indices;
	std::vector<Animation> animations;
	std::vector<BoneAnimation> bone_animations;
	std::vector<Bone> bones;
	std::vector<BoneHandle> bone_handles;
	SceneNode root;
	glm::mat4 global_inverse_transformation;
	SceneNode skeleton;
	FbxScene* scene;
	BoundingBox bounding_box;

private:
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);

	void clear();
	inline void map_bone_to_vertex(Vertex& vertex, int id, float weight)const;
	std::vector<Mesh3> meshes;
	
	Mesh3 load_mesh(const FbxMesh& mesh, const std::vector<Vertex>& control_points);
	Mesh3 load_mesh3(const FbxMesh& mesh, const std::vector<Vertex>& control_points);
	std::vector<Vertex> load_control_points(const FbxMesh& mesh);
	std::vector<Vertex>::const_iterator find_duplicate(const Mesh3& from, Vertex& what)const;

	void load_mesh(const FbxNode& node);
	std::vector<Mesh> combine_meshes(std::vector<Mesh3>& meshes);
	void fix_scene_after_mesh_combine(SceneNode& node);
	std::vector<std::string> get_texture_filenames(FbxProperty &pProperty);
	int get_material_id(const FbxMesh& mesh, int triangle_index);
	std::vector<Bone> load_bones(const FbxGeometry& geometry);
	std::vector<Bone> load_and_map_bones(const FbxGeometry& geometry, std::vector<Vertex>& control_points);
	
	void map_bones_to_vertex(const std::vector<Bone>& bones, Vertex& vertex, int old_indice);
	void map_bones_to_vertex(const FbxGeometry& geometry, Vertex& vertex, int old_indice);
	void traverse_node(const FbxNode& node, SceneNode& parent);
	void load_material(const FbxSurfaceMaterial& material, Material &material_out);
	void load_materials(FbxScene& scene);
	void remove_duplicate_materials(std::map<int, Material> &materials, std::vector<Mesh3> &meshes);
	std::map<int, std::vector<unsigned short>>::iterator find_mesh_using_material(int material_id);
	std::vector<std::map<int, Material>::const_iterator> find_duplicate_materials(const std::map<int, Material> &materials, const Material& reference_material);
	bool contains_node(const SceneNode& node, const std::string& nodename);
	SceneNode* find_node_using_mesh(SceneNode& root, int mesh_index);
	
	void get_min_max(const SceneNode& node, glm::vec3& min, glm::vec3& max)const;

	//animations
	bool all_key_transformations_identical(const std::vector<Animation::KeyMatrix>& keys);
	bool has_meshes(const SceneNode& scene_node, const FbxNode& node);
	std::vector<BoneAnimation> load_bone_animations2(FbxScene& scene);
	void remove_animations_that_dont_affect_scene(std::vector<Animation> &animations, const SceneNode& root);
	std::vector<Animation::Key> load_animation_keys(FbxAnimCurve& pCurve, float animation_length)const;
	std::vector<Animation::KeyMatrix> load_channel_transformation_keys(FbxNode& node, FbxAnimLayer& animation_layer, float animation_length)const;
	Animation load_animation_channel(FbxNode& node, FbxAnimLayer& animation_layer, float animation_length)const;
	void load_animation_layer(FbxAnimLayer& animation_layer, FbxNode& node, std::vector<Animation>& animations, float animation_length);
	void load_animation_stack(FbxAnimStack& animation_stack, FbxNode& node, std::vector<Animation>& animations);
	std::vector<Animation> load_animations(FbxScene& scene);
	std::vector<BoneAnimation> load_bone_animations(const FbxScene& scene);
	std::vector<BoneAnimation> load_bone_animations(const FbxNode& parent, const FbxNode& node);
	std::vector<BoneAnimation> load_bone_animations(const FbxGeometry& geometry);
	void try_find_animation_affecting_geometry(const FbxScene& scene, const FbxNode& node, FbxAnimStack*& result);
	
	
};

#endif

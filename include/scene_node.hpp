#ifndef SCENENODE_HPP
#define SCENENODE_HPP
#include <string>
#include <glm/glm.hpp>
#include <vector>
#include <iostream>

struct SceneNode
{
	std::string name;
	glm::mat4 transformation;
	std::vector<int> meshes;
	std::vector<SceneNode> children;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);
};

#endif

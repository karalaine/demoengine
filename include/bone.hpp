#ifndef BONE_HPP
#define BONE_HPP
#include <string>
#include <vector>
#include <glm/glm.hpp>

struct VertexBoneData
{
	VertexBoneData(unsigned int vertex_id, float weight) : vertex_id(vertex_id), weight(weight){}
	VertexBoneData() : vertex_id(0), weight(0){}
	unsigned int vertex_id;
	float weight;
};	
struct Bone //used when loading the data
{
	Bone(const std::string& name, const glm::mat4& offset, const std::vector<VertexBoneData>& vertex_weights) : 
		name(name),
		offset(offset),
		vertex_weights(vertex_weights){}
	Bone(){}
	std::string name;
	glm::mat4 offset, transformation;
	glm::mat4 transform, transform_link, transform_associate_model, bind_matrix;
	std::vector<VertexBoneData> vertex_weights;
};
struct BoneHandle //need better name
{
	BoneHandle(){}
	BoneHandle(const std::string& name, const glm::mat4& offset) : 
		name(name), offset(offset)
	{
	}
	std::string name;
	glm::mat4 offset, transformation;
	void serialize(std::ostream& out)const;
	void serialize(std::istream& in);
};

#endif

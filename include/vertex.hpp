#ifndef VERTEX_HPP
#define VERTEX_HPP
#include <glm/glm.hpp>

struct Vertex
{
	Vertex(const glm::vec3& vertex, const glm::vec3& normal, const glm::vec3& tex_coord, const glm::vec3& tangent, 
		const glm::ivec4& bone_ids, const glm::vec4& bone_weights) :
		vertex(vertex), normal(normal), tex_coord(tex_coord), tangent(tangent), bone_ids(bone_ids), bone_weights(bone_weights)
	{
	}
	Vertex(){}
	glm::vec3 vertex, normal, tangent;
	glm::vec2 tex_coord;
	glm::ivec4 bone_ids;
	glm::vec4 bone_weights;
};
bool fast_vector_compare(const glm::vec4& left, const glm::vec4& right);
bool fast_vector_compare(const glm::vec3& left, const glm::vec3& right);
bool fast_vector_compare(const glm::vec2& left, const glm::vec2& right);
bool operator ==(const Vertex& left, const Vertex& right);

#endif

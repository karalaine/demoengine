#pragma once
#include "Shader.h"
#include <memory>
namespace demo
{
	class Engine
	{
		public:
			Engine(void);
			virtual ~Engine(void);
			int run();
		private:
			void render();
	};
}

